module gitlab.com/indelibl/indelbl-api

go 1.15

require (
	github.com/fatih/structs v1.1.0
	github.com/gin-contrib/pprof v1.3.0
	github.com/gin-gonic/gin v1.7.4
	github.com/go-playground/validator/v10 v10.9.0 // indirect
	github.com/go-redis/redis/v8 v8.11.3
	github.com/ipfs/go-ipfs v0.9.1
	github.com/ipfs/go-ipfs-config v0.16.0
	github.com/ipfs/go-ipfs-files v0.0.8
	github.com/ipfs/interface-go-ipfs-core v0.5.1
	github.com/joho/godotenv v1.3.0
	github.com/libp2p/go-libp2p-core v0.9.0
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/multiformats/go-multiaddr v0.4.0
	github.com/ugorji/go v1.2.6 // indirect
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5 // indirect
	golang.org/x/sys v0.0.0-20210906170528-6f6e22806c34 // indirect
)

replace github.com/ipfs/go-ipfs => ./../github.com/ipfs/go-ipfs
