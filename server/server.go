package server

import (
	"github.com/gin-contrib/pprof"
	"github.com/gin-gonic/gin"
	"indeliblBackend/server/routes"
)

func CreateServer() *gin.Engine {
	r := gin.New()
	pprof.Register(r)
	routes.SetRoutes(r)
	return r
}
