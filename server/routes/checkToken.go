package routes

import (
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"net/http"
	"os"
	"time"
)

type googleResponse struct {
	Success     bool      `json:"success"`
	Score       float64   `json:"score"`
	Action      string    `json:"action"`
	ChallengeTS time.Time `json:"challenge_ts"`
	Hostname    string    `json:"hostname"`
	ErrorCodes  []string  `json:"error-codes"`
}

func CheckToken(action string) gin.HandlerFunc {
	return func(c *gin.Context) {

		grcToken := c.GetHeader("grctoken")
		if grcToken == "" {
			c.Error(gin.Error{
				Err:  errors.New("Forbidden: Invalid rechapta token"),
				Type: 403,
				Meta: nil,
			})
			return
		}
		req, err := http.NewRequest(http.MethodPost, "https://www.google.com/recaptcha/api/siteverify", nil)
		{
			if err != nil {
				c.Error(err)
				return
			}
			q := req.URL.Query()
			q.Add("secret", os.Getenv("GCHAPTA_SECRET"))
			q.Add("response", grcToken)
			req.URL.RawQuery = q.Encode()
			resp, err := http.DefaultClient.Do(req)
			{
				if err != nil {
					c.Error(err)
					return
				}

				var body *googleResponse
				if err = json.NewDecoder(resp.Body).Decode(&body); err != nil {
					c.Error(err)
					return
				}

				if !body.Success || body.Action != action || body.Score < 0.5 {
					c.Error(errors.New("Invalid url "))
					return
				}
				c.Next()
			}

		}

	}

}
