package routes

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func mainRoute(rg *gin.RouterGroup) *gin.RouterGroup {

	rg.GET("/", func(context *gin.Context) {
		context.String(http.StatusOK, "Running")
	})
	return rg
}

func SetRoutes(ge *gin.Engine) *gin.Engine {

	mainRoute(ge.Group("/")).Use(ErrHandler)

	geGroup := ge.Group("/r")
	geGroup.Use(gin.Recovery())
	{
		geGroup.Use(SetHeaders)

		geGroup.OPTIONS("", func(context *gin.Context) {
			context.String(http.StatusOK, "")
		}, ErrHandler)
		geGroup.POST("", CheckToken("scrape"), ErrHandler, CreateRequest())

	}

	return ge
}
