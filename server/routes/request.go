package routes

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/fatih/structs"
	gin "github.com/gin-gonic/gin"
	"indeliblBackend/lib"
	"io/ioutil"
	"net/http"
	url2 "net/url"
	"os"
)

type reqBody struct {
	Url string `json:"url" binding:"required"`
}

func CreateRequest() gin.HandlerFunc {
	return func(context *gin.Context) {
		defer func() {

			if err := recover(); err != nil {
				context.Error(gin.Error{
					Err:  err.(error),
					Type: 0,
					Meta: nil,
				}).SetMeta(err)
				return
			}
		}()
		var a reqBody
		err := context.ShouldBindJSON(&a)
		if err != nil {
			panic(err)
		}
		url, err := parseUrl(&a.Url)
		if err != nil {
			panic(err)
		} else {

			db := lib.NewDB()
			id := db.SetRequest(url)
			resp := structs.Map(url)
			resp["id"] = id
			context.IndentedJSON(http.StatusOK, resp)

		}

		request2Crawler(url)

	}

}

func request2Crawler(url *url2.URL) ([]byte, error) {
	body, _ := json.Marshal(
		struct {
			Url string `json:"url"`
		}{
			Url: url.String(),
		})
	resp, _ := http.Post(os.Getenv("SCRAPER_URL"),
		"application/json",
		bytes.NewBuffer(body))

	respBody, _ := ioutil.ReadAll(resp.Body)
	fmt.Print(respBody)

	return nil, nil
}

func parseUrl(str *string) (*url2.URL, error) {

	url, err := url2.Parse(*str)
	//if err != nil {
	//	 return  nil , err
	//}
	if url.Scheme != "http" && url.Scheme != "https" {
		panic(errors.New("Invalid Url"))
	}

	return url, err
}
