package routes

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func ErrHandler(c *gin.Context) {
	c.Next()
	detectedErrors := c.Errors
	if len(detectedErrors) > 0 {
		c.IndentedJSON(http.StatusBadRequest, detectedErrors)
		c.Abort()
	}

}
