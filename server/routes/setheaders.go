package routes

import (
	"github.com/gin-gonic/gin"
)

func SetHeaders(c *gin.Context) {

	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Headers", "*")
	c.Next()
}
