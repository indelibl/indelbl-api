package main

import (
	"context"
	"errors"
	"fmt"
	"github.com/joho/godotenv"
	"indeliblBackend/lib"
	"indeliblBackend/server"
	"log"
	"net/http"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	//loop:
	//	for {
	//		select {
	//		case <- ctx.Done():
	//			fmt.Print("exiting")
	//			break loop
	//		default:
	//			time.Sleep(1000)
	//		}
	//	}
	//ctx.Done()
	cli := lib.Load(&ctx)
	//_, _ = cli.GetHash("QmXUfXd6ztykdKkWTuEuwAdpERjAJhTkpe8v7kUsKTWdj5")
	hash, err := cli.AddFile("./data/tesla.jpg")
	if err != nil {
		panic(err)
	}

	fmt.Print(hash)
	godotenv.Load(".env.dev")
	server := server.CreateServer()

	srv := &http.Server{
		Addr:    ":8080",
		Handler: server,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && errors.Is(err, http.ErrServerClosed) {
			log.Printf("listen: %s\n", err)
		}
	}()
	go heartBeat(cli, &ctx)
	//quit := make(chan os.Signal)
	//signal.Notify(quit, os.Interrupt)
	//   <-quit
	<-ctx.Done()
	fmt.Print("exiting")
}

func heartBeat(cli *lib.IPFSCli, ctx *context.Context) {
	for range time.Tick(time.Second * 1) {
		wd, _ := cli.CoreAPI.Swarm().Peers(*ctx)
		println(wd)
	}
}
