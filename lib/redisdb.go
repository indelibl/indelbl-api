package lib

import (
	"context"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
	url2 "net/url"
)

var connection *redis.Client
var ctx = context.Background()

type DB struct {
	conn *redis.Client
}

func NewDB() *DB {
	if connection == nil {
		connection = redis.NewClient(&redis.Options{
			Addr:     "localhost:6379",
			Password: "", // no password set
			DB:       0,  // use default DB
		})
	}
	p := new(DB)
	p.conn = connection
	return p
}

func (db *DB) SetRequest(url *url2.URL) string {
	url.EscapedFragment()
	timeNow := time.Now().Unix()
	db.conn.SAdd(ctx, "r:host", url.Host)
	db.conn.SAdd(ctx, "r:path:"+url.Host, url.Path+url.RawQuery)
	db.conn.Set(ctx, "r:record:"+url.Host+url.Path+url.RawQuery, string(timeNow), 0)
	return strconv.FormatInt(timeNow, 10)
}
